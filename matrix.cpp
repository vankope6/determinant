#include <iostream>
#include <vector>
#include "matrix.hpp"

matrix::matrix(int dimension, long double diag) :
    dimension(dimension)
    {
    data = new long double[dimension * dimension]();
    for (int i = 0; i < dimension; i++){
        data[i*dimension + i] = diag;
    }
}

matrix::matrix(const matrix &A) {
    this->dimension = A.dimension;
    this->data = new long double[dimension * dimension]();
    for (int i = 0; i < dimension*dimension; i++){
        this->data[i] = A.data[i];
    }
}

matrix::~matrix() {
    delete [] data;
}

void matrix::fill(ifstream &in) {
    for (int i = 0; i < dimension; i++){
        for (int j = 0; j < dimension; j++){
            in >> data[i*dimension + j];
        }
    }
}

long double matrix::determinant() {
    matrix L(dimension, 1);
    matrix U(*this);
    int P_rows_swap = 0;
    P_rows_swap = LU_decomp(L, U);
    if (P_rows_swap % 2 == 1) {
        return (-1) * L.diag_det() * U.diag_det();
    }

    return L.diag_det()*U.diag_det();
}

int matrix::LU_decomp(matrix &L, matrix &U) {
    int P_rows_swap = 0;
    // for each column
    for (int i = 0; i < dimension - 1; i++){
        // set the diagonal value as pivot
        long double pivot = U.data[i*dimension + i];
        int p = i;
        // find the smallest non-zero value below the pivot
        for (int k = i + 1; k < dimension; k++){
            // if there is a smaller non-zero value below the pivot, set new pivot and remember its row (p)
            if ((pivot == 0 && (U.data[k*dimension + i] > 0)) || (U.data[k*dimension + i] < pivot && (U.data[k*dimension + i] > 0))){
                pivot = U.data[k*dimension + i];
                p = k;
            }
        }
        // if there is a smallest value below the diagonal, swap the two rows, so that the new pivot is at diagonal
        if (p > i){
            U.swap_rows(i, p);
            P_rows_swap++;
            long double *temp_row;
            temp_row = new long double[dimension]();
            for (int j = 0; j < i; j++){
                temp_row[j] = L.data[i*dimension + j];
                L.data[i*dimension + j] = L.data[p*dimension + j];
                L.data[p*dimension + j] = temp_row[j];
            }
            delete[] temp_row;
        }
        // for each row below the pivot
        for (int k = i + 1; k < dimension; k++){
            // if value is 0, do nothing
            if (U.data[k*dimension + i] == 0){
                continue;
            }
            // else do Gaussian elimination to get a upper-triangle U matrix
            long double div = -U.data[k*dimension + i]/U.data[i*dimension + i];
            for (int j = 0; j < dimension; j++){
                U.data[k*dimension + j] = U.data[k*dimension + j] + div*U.data[i*dimension + j];
            }
            L.data[k*dimension + i] = -div;
        }
    }
    return P_rows_swap;
}

long double matrix::diag_det() {
    long double det = 1;
    for (int i = 0; i < dimension; i++){
        det *= data[i*dimension + i];
    }
    return det;
}

void matrix::swap_rows(int row1, int row2) {
    long double *temp_row;
    temp_row = new long double[dimension]();
    for (int i = 0; i < dimension; i++){
        temp_row[i] = data[row1*dimension + i];
        data[row1*dimension + i] = data[row2*dimension + i];
        data[row2*dimension + i] = temp_row[i];
    }
    delete[] temp_row;
}

ostream& operator<<(ostream &out, matrix &m) {
    for (int i = 0; i < m.dimension; i++) {
        for (int j = 0; j < m.dimension; j++) {
            out << m.data[i*m.dimension + j] << " ";
        }
        out << endl;
    }
    return out;
}