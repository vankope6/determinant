#ifndef DET_MATRIX_HPP
#define DET_MATRIX_HPP

#include <fstream>

using namespace std;

class matrix {
public:
    // matrix constructor, accepts matrix dimension and diagonal value
    explicit matrix(int dimension, long double diag);
    // copy constructor, create a new matrix by copying dimension and values from already existing one
    matrix(const matrix &A);
    // matrix destructor
    ~matrix();

    // fills matrix data from file
    void fill(ifstream& in);

    // computes a determinant from LU decomposition
    long double determinant();

    // prints matrix in a readable format, used for debugging
    friend ostream& operator<<(ostream &os, matrix &m);
private:
    // computes LU decomposition of matrix, returns number of row swaps in matrix P
    int LU_decomp(matrix &L, matrix &U);
    // swaps two rows in a matrix
    void swap_rows(int row1, int row2);
    // computes a diagonal determinant = multiplicates values at the diagonal of a matrix
    long double diag_det();
    // dimension of matrix
    int dimension;
    // values stored in an array
    long double* data;
};

ostream& operator<<(ostream &os, matrix &m);

#endif //DET_MATRIX_HPP
