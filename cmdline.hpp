#pragma once

#include <ostream>

using namespace std;

// read command, handle flags, compute determinant, return exitstatus
int handle_command(ostream &os, int argc, char *argv[]);

// print help message
void show_help();

//print long double number in readable format
void pretty_print(long double number);