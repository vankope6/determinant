# Determinant čtvercové matice

Tato závěrečná zpráva se vztahuje ke commitu s tagem `v1.0.0`.

Využití šuplíkového zadání: 
#### Výpočet determinantu čtvercové matice

Determinant lze jednoduše počítat dle definice nebo Laplaceovou expanzí, v praxi tyto algoritmy ale nejsou vhodné kvůli své časové složitosti. Efektivní implementace jsou založené na Gaussově eliminační mětodě (LU dekompozice). Očekáváme, že váš program bude schopen pracovat s maticemi 1000×1000 v rozumném čase (tj. řádově vteřiny, max. desítky vteřin). Aplikace na vstupu dostane čtvercovou matici, na výstupu vypíše hodnotu jejího determinantu.

## Zpracování
Aplikace má pouze jednovláknovou variantu. Vícevláknová varianta se mi bohužel nepodařila implementovat tak, aby výrazně zkrátila dobu běhu programu.

#### Spuštění aplikace
K aplikace je přiložen `CMakeLists.txt` soubor, pomocí kterého je možné aplikaci postavit. Následné spuštění přes příkazovou řádku má následující možnosti:

```
Usage options:
     -h,--help      Show this help message
     -f,--file      Specify the input data file path
```  
Tato zpráva se vypíše také při nesprávném spuštění aplikace (neznámé parametry, žádné parametry apod.).

#### Vstupní soubor
Na vstupu je očekávána cesta k souboru, který obsahuje čtvercovou matici, jejíž determinant následně vypíše. Na prvním řádku souboru je očekáván rozměr matice N a na dalších N řádcích samotná data.

**Příklad souboru**:
```
3
4 3 -1
3 5 3
1 1 1
``` 

Ve složce `data` jsou přiloženy ukázkové soubory s testovacími daty. Do rozměru 15x15 jsou přiložené i výsledné determinanty (v souboru `data_XxX_out.txt`), jejichž správnost jsem ověřovala [zde](https://matrix.reshish.com/detCalculation.php). Pro vyšší rozměry jsem bohužel nenašla odpovídající zdroj (buď tak velké matice nepodporují, výpočetně nezvládnou, nebo není výstup v žádoucím formátu).
#### Výpočet
Výpočet determinantu matice probíhá pomocí LU rozkladu s pivotací, tzn. že v průběhu rozkladu jsou prohazovány řádky a výsledný determinant je pak ovlivněn i determinantem permutační matice.

#### Implementace
Pro jednodušší práci s maticí jsem vytvořila třídu `matrix`, která obsahuje proměnnou `dimension`, tedy rozměr, a `data`, což je pole hodnot matice. Kromě konstruktorů a destruktoru obsahuje třída také veřejné metody `fill()`, která naplní proměnnou data hodnotami ze vstupního souboru a metodu `determinant()`, která spočítá a vrátí determinant příslušné matice.

Dále třída obsahuje private metody, které jsou volány při výpočtu determinantu:
 * `LU_decomp()` - provede LU dekompozici a vrátí počet prohození řádků (místo počítání celé permutační matice)
 * `swap_rows()` - prohodí dva řádky příslušné matice
 * `diag_det()` - vynásobí prvky na diagonále = determinant diagonální matice

Třída také obsahuje metodu, která vytiskne matici na výstup v čitelném formátu. Tato metoda sloužila pouze k testování.