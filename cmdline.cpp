#include <iostream>
#include "cmdline.hpp"
#include "matrix.hpp"

using namespace std;

int handle_command(ostream &os, int argc, char *argv[]){
    if (argc == 2 && (string(argv[1]) == "--help" || string(argv[1]) == "-h")){
        show_help();
        return 0;
    } else if (argc == 3 && (string(argv[1]) == "--file" || string(argv[1]) == "-f")){
        try {
            ifstream infile(argv[2]);
            if (!infile.is_open()){
                throw std::runtime_error("file does not exist");
            }
            int dim = 0;
            infile >> dim;
            matrix m(dim,0);
            m.fill(infile);
            long double det = m.determinant();
            pretty_print(det);
            return 0;
        } catch (const exception &e){
            os << "Error reading file: " << argv[2] << endl;
            return -1;
        }
    } else {
        os << "Wrong usage, see:" << std::endl;
        show_help();
        return 1;
    }
}

void pretty_print(long double number) {
    printf("%.0Lf\n", number);
}


void show_help() {
    std::cout << "Usage options:\n"
              << "\t-h,--help\tShow this help message\n"
              << "\t-f,--file\tSpecify the input data file path"
              << std::endl;
}