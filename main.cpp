#include <iostream>
#include "cmdline.hpp"
using namespace std;

int main(int argc, char *argv[]){
    int exitcode = -1;
    exitcode = handle_command(cout, argc, argv);
    return exitcode;
}